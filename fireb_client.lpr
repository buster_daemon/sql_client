program fireb_client;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, MainWindow, aboutgpl, aboutprogram;

{$R *.res}

begin
  RequireDerivedFormResource:=True;
  Application.Title:='SQuirreL';
  Application.Scaled:=True;
  Application.Initialize;
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TGPLForm, GPLForm);
  Application.CreateForm(TAboutPgrForm, AboutPgrForm);
  Application.Run;
end.


unit MainWindow;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, SQLDB, SQLite3Conn, mysql57conn, PQConnection, DB,
  IBConnection, Forms, Controls, Graphics, Dialogs, DBGrids, DBCtrls, StdCtrls,
  Buttons, ExtCtrls, Menus, StrUtils, DefaultTranslator, ComCtrls, AboutGPL,
  AboutProgram;

type

  { TMainForm }

  TMainForm = class(TForm)
    AdvancedDBGrid: TDBGrid;
    AdvDBDataSource: TDataSource;
    AdvDBTransBtn: TButton;
    ExScriptBtn: TButton;
    ClrScriptBtn: TButton;
    DBTableName: TComboBox;
    DBtblNameLbl: TLabel;
    FileMen: TMenuItem;
    DisconnectMen: TMenuItem;
    AdvScriptField: TPanel;
    ProceduresListBox: TListBox;
    ProcedListPan: TPanel;
    ScriptFieldMem: TMemo;
    MenuItem1: TMenuItem;
    AboutMen: TMenuItem;
    AboutGPLItm: TMenuItem;
    AboutAuthItm: TMenuItem;
    DBTabs: TPageControl;
    QuitMen: TMenuItem;
    DBTab: TTabSheet;
    DBTabBottomSplit: TSplitter;
    DBTabLeftSplit: TSplitter;
    DBTabRightSplit: TSplitter;
    DBAdvancedOpts: TTabSheet;
    AdvDBLeftSplitter: TSplitter;
    AdvDBRightSplitter: TSplitter;
    AdvDBTopSplitter: TSplitter;
    AdvDBBottSplitter: TSplitter;
    CMDSplitter: TSplitter;
    AdvSelectQuery: TSQLQuery;
    AdvEditQuery: TSQLQuery;
    AdvSQLScript: TSQLScript;
    AdvDBRG: TRadioGroup;
    AdvDBRGSplitter: TSplitter;
    ScriptLeftSplitter: TSplitter;
    ScriptTopSplitter: TSplitter;
    ScriptRightSplitter: TSplitter;
    ScriptBotSplitter: TSplitter;
    FieldSplitter: TSplitter;
    Menus: TMainMenu;
    MSQLCon: TMySQL57Connection;
    DBSelDial: TOpenDialog;
    DBNameBox: TLabeledEdit;
    LeftSplit: TSplitter;
    BottomSplit: TSplitter;
    SelectDBBtn: TBitBtn;
    ConnectBtn: TButton;
    DisconBtn: TButton;
    SQLComExec: TButton;
    SQLLCon: TSQLite3Connection;
    UpdateBtn: TButton;
    DBSelBox: TComboBox;
    DataSource1: TDataSource;
    DBGrid1: TDBGrid;
    DBNavigator1: TDBNavigator;
    SQLCommB: TEdit;
    FireBirdLocal: TIBConnection;
    SQLComLbl: TLabel;
    RightSplit: TSplitter;
    TopSplit: TSplitter;
    SQLDBPath: TLabeledEdit;
    SQLPortBox: TLabeledEdit;
    SQLAdBox: TLabeledEdit;
    PostgreCon: TPQConnection;
    SQLBoxCapt: TLabel;
    LoginEdBox: TLabeledEdit;
    PasswEdBox: TLabeledEdit;
    SQLCommCls: TSpeedButton;
    SelectQuery: TSQLQuery;
    SQLTransaction1: TSQLTransaction;
    procedure AboutAuthItmClick(Sender: TObject);
    procedure AboutGPLItmClick(Sender: TObject);
    procedure AdvDBRGClick(Sender: TObject);
    procedure AdvDBTransBtnClick(Sender: TObject);
    procedure ClrScriptBtnClick(Sender: TObject);
    procedure ConnectBtnClick(Sender: TObject);
    procedure DBSelBoxChange(Sender: TObject);
    procedure DBTableNameChange(Sender: TObject);
    procedure DisconBtnClick(Sender: TObject);
    procedure DisconnectMenClick(Sender: TObject);
    procedure ExScriptBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QuitMenClick(Sender: TObject);
    procedure SQLComExecClick(Sender: TObject);
    procedure UpdateBtnClick(Sender: TObject);
    procedure OpTabClick();
    procedure SelectDBBtnClick(Sender: TObject);
    procedure SQLCommClsClick(Sender: TObject);
    function SQLOp(): boolean;
    procedure SQLCommit();
    procedure DisableSomeBtns(status: boolean);
    procedure SQLConsole();
    function EnableTransactions(status: boolean): boolean;
    procedure StartCommits();
  private

  public

  end;

var
  MainForm: TMainForm;
  connect_status: boolean = false;

resourcestring
  EmptyLoginText = 'You must enter a login before connecting.';
  EmptyPasswText = 'You must enter the password before connecting.';
  EmptyDBNameText = 'You must enter the name of the database to connect.';
  EmptyDBAdText = 'You must enter the database address to connect.';
  EmptyDBPathText = 'You must specify the path to the database to connect.';
  ConErrorText = 'The connection to the database failed. Make sure that all the credentials you entered are correct.';
  StartTransErrorText = 'Transaction error, try to reconnect to the database.';
  StopTransErrorText = 'Transaction stop error.';
  TrebovanText = 'Please follow the above requirement.';
  DisconErrorText = 'You are not connected to any database to disconnect.';
  TransMsgTitle = 'Transaction confirmation';
  TransMsgText = 'Do you really want to apply the changes to the database? Otherwise they will be undone.';
  TransComError = 'Error in applying changes.';
  TransRolError = 'Error undoing changes.';
  QueryErrorText = 'Query execution error.';
  CmdErrorText = 'The entered code is invalid.';
  ScriptErrorMsg = 'Error during script execution.';
  DBRGItem1 = 'Execute SQL Command';
  DBRGItem2 = 'Execute SQL Script';
  DBRGItem3 = 'List of DB Procedures';

implementation

{$R *.lfm}

{ TMainForm }

function TMainForm.SQLOp(): boolean;
begin
  if (LoginEdBox.Text='') and not (DBSelBox.ItemIndex=1) then begin
  ShowMessage(EmptyLoginText);
  exit(false);
  end
  else if (PasswEdBox.Text='') and not (DBSelBox.ItemIndex=1) then begin
  ShowMessage(EmptyPasswText);
  exit(false);
  end
  else if (DBNameBox.Text='') then begin
  ShowMessage(EmptyDBNameText);
  exit(false);
  end;
  if (DBSelBox.ItemIndex>1) and (SQLAdBox.Text='') then begin
  ShowMessage(EmptyDBAdText);
  exit(false);
  end
  else if (SQLDBPath.Text='') and (DBSelBox.ItemIndex<2) then begin
  ShowMessage(EmptyDBPathText);
  exit(false);
  end
  else begin
  if DBSelBox.ItemIndex=0 then begin
  FireBirdLocal.UserName:=LoginEdBox.Text;
  FireBirdLocal.Password:=PasswEdBox.Text;
  FireBirdLocal.DatabaseName:=SQLDBPath.Text;
  end
  else if DBSelBox.ItemIndex=2 then begin
  PostgreCon.DatabaseName:=DBNameBox.Text;
  PostgreCon.UserName:=LoginEdBox.Text;
  PostgreCon.Password:=PasswEdBox.Text;
  if not (SQLPortBox.Text='') then PostgreCon.Params.Add('port='+SQLPortBox.Text);
  PostgreCon.HostName:=SQLAdBox.Text;
  end
  else if DBSelBox.ItemIndex=3 then begin
  MSQLCon.DatabaseName:=DBNameBox.Text;
  MSQLCon.UserName:=LoginEdBox.Text;
  MSQLCon.Password:=PasswEdBox.Text;
  MSQLCon.HostName:=SQLAdBox.Text;
  MSQLCon.Port:=StrToInt(SQLPortBox.Text);
  end
  else if DBSelBox.ItemIndex=1 then begin
  SQLLCon.DatabaseName:=SQLDBPath.Text;
  if not (LoginEdBox.Text='') then SQLLCon.UserName:=LoginEdBox.Text;
  if not (PasswEdBox.Text='') then SQLLCon.Password:=PasswEdBox.Text;
  end;
  if DBSelBox.ItemIndex=0 then
  try
    FireBirdLocal.Connected:=True;
    SQLTransaction1.DataBase:=FireBirdLocal;
    SelectQuery.DataBase:=FireBirdLocal;
    DBTableName.Clear;
    AdvSelectQuery.DataBase:=FireBirdLocal;
    AdvEditQuery.DataBase:=FireBirdLocal;
    AdvSQLScript.DataBase:=FireBirdLocal;
    ProceduresListBox.Clear;
    try
      FireBirdLocal.GetTableNames(DBTableName.Items,false);
    except
      FireBirdLocal.GetTableNames(DBTableName.Items,true);
    end;
    try
      FireBirdLocal.GetProcedureNames(ProceduresListBox.Items);
    except
    end;
    DBTableName.ItemIndex:=0;
    except
      ShowMessage(ConErrorText);
    end
  else if DBSelBox.ItemIndex=2 then
  try
    PostgreCon.Connected:=true;
    SQLTransaction1.DataBase:=PostgreCon;
    SelectQuery.DataBase:=PostgreCon;
    DBTableName.Clear;
    AdvSelectQuery.DataBase:=PostgreCon;
    AdvEditQuery.DataBase:=PostgreCon;
    AdvSQLScript.DataBase:=PostgreCon;
    ProceduresListBox.Clear;
    try
      PostgreCon.GetTableNames(DBTableName.Items,false);
    except
      PostgreCon.GetTableNames(DBTableName.Items,true);
    end;
    try
      PostgreCon.GetProcedureNames(ProceduresListBox.Items);
    except
    end;
    DBTableName.ItemIndex:=0;
  except
    ShowMessage(ConErrorText);
  end
  else if DBSelBox.ItemIndex=3 then
  try
    MSQLCon.Connected:=true;
    SQLTransaction1.DataBase:=MSQLCon;
    SelectQuery.DataBase:=MSQLCon;
    DBTableName.Clear;
    AdvSelectQuery.DataBase:=MSQLCon;
    AdvEditQuery.DataBase:=MSQLCon;
    AdvSQLScript.DataBase:=MSQLCon;
    ProceduresListBox.Clear;
    try
      MSQLCon.GetTableNames(DBTableName.Items,false);
    except
      MSQLCon.GetTableNames(DBTableName.Items,true);
    end;
    try
      MSQLCon.GetProcedureNames(ProceduresListBox.Items);
    except
    end;
    DBTableName.ItemIndex:=0;
  except
    ShowMessage(ConErrorText);
  end
  else if DBSelBox.ItemIndex=1 then
  try
    SQLLCon.Connected:=true;
    SQLTransaction1.DataBase:=SQLLCon;
    SelectQuery.DataBase:=SQLLCon;
    DBTableName.Clear;
    AdvEditQuery.DataBase:=SQLLCon;
    AdvSelectQuery.DataBase:=SQLLCon;
    AdvSQLScript.DataBase:=SQLLCon;
    try
      SQLLCon.GetTableNames(DBTableName.Items,false);
    except
      SQLLCon.GetTableNames(DBTableName.Items,true);
    end;
    DBTableName.ItemIndex:=0;
  except
    ShowMessage(ConErrorText);
  end;
  end;
  exit(true);
end;

function TMainForm.EnableTransactions(status: boolean): boolean;
begin
  if status=true then
  try
    SQLTransaction1.Active:=true;
    SelectQuery.Active:=false;
    SelectQuery.sql.Clear;
    connect_status:=true;
    AdvSelectQuery.Active:=false;
    AdvEditQuery.Active:=false;
    AdvSelectQuery.sql.clear;
    AdvEditQuery.sql.clear;
    exit(true)
  except
    ShowMessage(ConErrorText);
    exit(false);
  end
  else
  try
    SQLTransaction1.Active:=false;
    SelectQuery.Active:=false;
    SelectQuery.sql.Clear;
    AdvSelectQuery.Active:=false;
    AdvEditQuery.Active:=false;
    AdvSelectQuery.sql.clear;
    AdvEditQuery.sql.clear;
    connect_status:=false;
    exit(true)
  except
    ShowMessage(ConErrorText);
    exit(false);
  end
end;

procedure TMainForm.ConnectBtnClick(Sender: TObject);
var
  s: boolean;
  t: boolean;
begin
  s:=SQLOp();
  if s=true then t:=EnableTransactions(true) else ShowMessage(TrebovanText);
  if t=true then
  begin
  DisableSomeBtns(true);
  DBTabs.Visible:=true;
  DBNavigator1.Visible:=true;
  UpdateBtn.Visible:=true;
  OpTabClick();
  end;
end;

procedure TMainForm.AboutGPLItmClick(Sender: TObject);
begin
  GPLForm.ShowModal;
end;

procedure TMainForm.AdvDBRGClick(Sender: TObject);
begin
  if AdvDBRG.ItemIndex=0 then begin
  AdvScriptField.Visible:=false;
  ProcedListPan.Visible:=false;
  SQLCommB.Visible:=true;
  SQLComLbl.Visible:=true;
  SQLComExec.Visible:=true;
  SQLCommCls.Visible:=true
  end
  else if advdbrg.ItemIndex=1 then begin
  AdvScriptField.Visible:=true;
  ProcedListPan.Visible:=false;
  SQLCommB.Visible:=false;
  SQLComLbl.Visible:=false;
  SQLComExec.Visible:=false;
  SQLCommCls.Visible:=false
  end
  else if advdbrg.ItemIndex=2 then begin
  AdvScriptField.Visible:=false;
  ProcedListPan.Visible:=true;
  SQLCommB.Visible:=false;
  SQLComLbl.Visible:=false;
  SQLComExec.Visible:=false;
  SQLCommCls.Visible:=false;
  end;
end;

procedure TMainForm.AdvDBTransBtnClick(Sender: TObject);
begin
  StartCommits();
end;

procedure TMainForm.ClrScriptBtnClick(Sender: TObject);
begin
  ScriptFieldMem.Clear;
end;

procedure TMainForm.AboutAuthItmClick(Sender: TObject);
begin
  AboutPgrForm.ShowModal;
end;

procedure TMainForm.DBSelBoxChange(Sender: TObject);
begin
  if DBSelBox.ItemIndex=0 then
  begin
  SQLDBPath.Visible:=True;
  SQLAdBox.Visible:=False;
  SelectDBBtn.Visible:=True;
  DBNameBox.Visible:=False;
  SQLPortBox.Visible:=False
  end
  else if DBSelBox.ItemIndex=2 then
  begin
  SQLDBPath.Visible:=False;
  SQLAdBox.Visible:=True;
  SelectDBBtn.Visible:=False;
  DBNameBox.Visible:=True;
  SQLPortBox.Visible:=True
  end
  else if DBSelBox.ItemIndex=3 then
  begin
  SQLDBPath.Visible:=False;
  SQLAdBox.Visible:=True;
  SelectDBBtn.Visible:=False;
  DBNameBox.Visible:=True;
  SQLPortBox.Visible:=True
  end
  else if DBSelBox.ItemIndex=1 then
  begin
  SQLDBPath.Visible:=True;
  SQLAdBox.Visible:=False;
  SelectDBBtn.Visible:=True;
  DBNameBox.Visible:=False;
  SQLPortBox.Visible:=False;
  end;
end;

procedure TMainForm.DBTableNameChange(Sender: TObject);
begin
  OpTabClick();
end;

procedure TMainForm.DisconBtnClick(Sender: TObject);
begin
  SQLCommit();
  DisableSomeBtns(false);
  DBTabs.Visible:=false;
  DBNavigator1.Visible:=false;
  UpdateBtn.Visible:=false;
  EnableTransactions(false);
  if DBSelBox.ItemIndex=0 then
  begin
  FireBirdLocal.Connected:=false;
  FireBirdLocal.UserName:='';
  FireBirdLocal.Password:='';
  FireBirdLocal.DatabaseName:='';
  end
  else if DBSelBox.ItemIndex=2 then begin
  PostgreCon.Connected:=false;
  PostgreCon.HostName:='';
  PostgreCon.UserName:='';
  PostgreCon.DatabaseName:='';
  PostgreCon.Password:='';
  PostgreCon.Params.Clear;
  end
  else if DBSelBox.ItemIndex=3 then begin
  MSQLCon.Connected:=false;
  MSQLCon.UserName:='';
  MSQLCon.Password:='';
  MSQLCon.HostName:='';
  MSQLCon.DatabaseName:='';
  end
  else if DBSelBox.ItemIndex=1 then begin
  SQLLCon.Connected:=false;
  if not (LoginEdBox.Text='') then SQLLCon.UserName:='';
  if not (PasswEdBox.Text='') then SQLLCon.Password:='';
  end;
end;

procedure TMainForm.DisconnectMenClick(Sender: TObject);
begin
  if connect_status=true then DisconBtnClick(MainForm) else ShowMessage(DisconErrorText);
end;

procedure TMainForm.ExScriptBtnClick(Sender: TObject);
begin
  try;
  AdvSQLScript.Script:=ScriptFieldMem.Lines;
  AdvSQLScript.ExecuteScript;
  except
    ShowMessage(ScriptErrorMsg);
  end;
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
  AdvDBRG.Items.Add(DBRGItem1);
  AdvDBRG.Items.Add(DBRGItem2);
  AdvDBRG.Items.Add(DBRGItem3);
  AdvDBRG.ItemIndex:=0;
end;

procedure TMainForm.QuitMenClick(Sender: TObject);
begin
  if connect_status=true then DisconBtnClick(MainForm);
  Application.Terminate;
end;

procedure TMainForm.SQLComExecClick(Sender: TObject);
begin
  if not (AnsiStartsText('select', SQLCommB.Text)=true) then begin
  SQLConsole();
  OpTabClick()
  end
  else SQLConsole();
end;

procedure TMainForm.UpdateBtnClick(Sender: TObject);
begin
  StartCommits();
end;

procedure TMainForm.OpTabClick();
begin
  try
    SelectQuery.Active:=false;
    SelectQuery.sql.Clear;
    SelectQuery.sql.add('select * from ' + DBTableName.Text);
    SelectQuery.Open;
  except
    ShowMessage(QueryErrorText);
  end;
end;

procedure TMainForm.SelectDBBtnClick(Sender: TObject);
begin
  if DBSelDial.Execute then SQLDBPath.Text:=DBSelDial.FileName;
end;

procedure TMainForm.SQLCommClsClick(Sender: TObject);
begin
  SQLCommB.Text:='';
end;

procedure TMainForm.SQLCommit();
begin
  SelectQuery.Active:=false;
  SQLTransaction1.Commit;
  PostgreCon.Connected:=false;
  FireBirdLocal.Connected:=false;
  SQLTransaction1.Active:=false;
  DisableSomeBtns(false);
end;

procedure TMainForm.DisableSomeBtns(status: boolean);
begin
  if status=true then begin
  LoginEdBox.Enabled:=False;
  PasswEdBox.Enabled:=False;
  DBNameBox.Enabled:=False;
  ConnectBtn.Enabled:=false;
  SQLAdBox.Enabled:=False;
  SQLDBPath.Enabled:=False;
  SQLPortBox.Enabled:=False;
  DBSelBox.Enabled:=False;
  SQLBoxCapt.Enabled:=false;
  SelectDBBtn.Enabled:=false;
  DisconBtn.Enabled:=true;
  DBTableName.Enabled:=true;
  SQLComLbl.Visible:=true;
  SQLCommB.Visible:=true;
  SQLComExec.Visible:=true;
  SQLCommCls.Visible:=true;
  UpdateBtn.Enabled:=true;
  DisconnectMen.Enabled:=true;
  end
  else begin
  LoginEdBox.Enabled:=True;
  PasswEdBox.Enabled:=True;
  SQLDBPath.Enabled:=True;
  DBSelBox.Enabled:=True;
  ConnectBtn.Enabled:=true;
  DBNameBox.Enabled:=true;
  SQLAdBox.Enabled:=true;
  SQLBoxCapt.Enabled:=true;
  SelectDBBtn.Enabled:=true;
  DisconBtn.Enabled:=false;
  DBTableName.enabled:=false;
  SQLComLbl.Visible:=false;
  SQLCommB.Visible:=false;
  SQLComExec.Visible:=false;
  SQLCommCls.Visible:=false;
  UpdateBtn.Enabled:=false;
  DisconnectMen.Enabled:=false;
  if DBSelBox.ItemIndex=0 then
  begin
  SQLDBPath.Enabled:=True;
  SQLPortBox.Enabled:=False;
  end
  else if DBSelBox.ItemIndex=2 then begin
  SQLAdBox.Enabled:=True;
  DBNameBox.Enabled:=True;
  SQLDBPath.Enabled:=False;
  SQLPortBox.Enabled:=True;
  end
  else if DBSelBox.ItemIndex=3 then begin
  SQLAdBox.Enabled:=True;
  DBNameBox.Enabled:=True;
  SQLDBPath.Enabled:=False;
  SQLPortBox.Enabled:=True;
  end
  else if DBSelBox.ItemIndex=1 then begin
  SQLDBPath.Enabled:=True;
  SQLPortBox.Enabled:=False;
  end;
end;
end;

procedure TMainForm.SQLConsole();
begin
  try
  if AnsiStartsText('select', SQLCommB.Text)=true then begin
  AdvSelectQuery.Active:=false;
  AdvSelectQuery.SQL.Clear;
  AdvSelectQuery.SQL.Add(SQLCommB.Text);
  AdvSelectQuery.Open;
  end
  else
  begin
  AdvEditQuery.Active:=false;
  AdvEditQuery.SQL.Clear;
  AdvEditQuery.SQL.Add(SQLCommB.Text);
  AdvEditQuery.ExecSQL;
  end
  except
    ShowMessage(CmdErrorText);
  end;
end;

procedure TMainForm.StartCommits();
begin
  if MessageDlg(TransMsgTitle,TransMsgText,mtConfirmation,[mbYes,mbNo],0) = mrYes then
  try
  SelectQuery.ApplyUpdates;
  SQLTransaction1.Commit;
  except
    ShowMessage(TransComError);
  end
  else
  try
  SQLTransaction1.Rollback;
  except
    ShowMessage(TransRolError);
  end;
  OpTabClick();
end;

end.

